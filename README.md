# IoT Predictive Maintenance Platform
## Scenario

<p align="center">
    <img src="images/architecture_1.JPG" width="100%" height="100%">
</p>

In the remote monitoring environment of the Internet of Things, you will learn how to build a **Predictive Maintenance (PdM, Predictive Maintenance)** using AWS's AI Cloud Service. Specifically, you will learn about generating data, feature extraction, training models, models deployment, and using training models to deploy website.

## Prerequisite

- Download [FileZilla](https://pdm-data.s3.amazonaws.com/FileZillaPortable.zip) to your computer and unzip it.
- Download [Necessary Files](https://pdm-data.s3.amazonaws.com/pdm_2019.zip) to your computer and unzip it.
- Download [PuTTY](https://the.earth.li/~sgtatham/putty/latest/w64/putty.exe), [PuTTYgen](https://the.earth.li/~sgtatham/putty/latest/w64/puttygen.exe) to your computer.
- Download **VPC.yaml** in this repository to your computer.
- The workshop’s region will be in **N.Virginia (us-east-1)**.

## Lab Tutorial

### Create VPC

Here we use the ClouFormation template **VPC.yaml** to setup your environment.

1. On the **Service** menu, select [CloudFormation](https://console.aws.amazon.com/cloudformation/).

2. Select **Create Stack**.

3. ☑ **Template is ready**.

4. In **Specify template**, ☑ **Upload a template file**.

5. Select **Choose file**, and select **VPC.yaml** and click **Next**.

<div>
<p align=center>
<img src="images/Cloudformation_Stack.jpg" width=80% height=80%>
</p>
</div>

6. Input `Lab-Stack-<your-name>` for the **Stack name**, select **Next**.
> Ex: `Lab-Stack-John`.

7. Leave the settings as default, select **Next**.

8. Review the details of this stack, if there is no problem select **Create stack**.

9. After creating stack, the name of VPC is `Lab VPC`.

### Create an EC2 Instance with user data
This instance is for compiling some required python file instead of compiling on your computer to avoid some troubles.

1. On the Service menu, select **EC2**.

2. On the left navigation pane, select **Instances**.

3. Select **Launch Instance**.

4. At the part of **Choose AMI**, select **Amazon Linux AMI**.
> **(Warning!!!)** If you choose **Amazon Linux 2 AMI**, the user data won't work.

5. Select **t2.micro** for **Instance Type**, and select **Next: Configure Instance Details**.

6. At the **Configure Instance Details** part, fill in these configurations:
    * Network: `Lab VPC`
    * Subnet: `Lab Public Subnet 1`
    * Auto-assign Public IP: `Enable`

7. Scroll down to the buttom of the page, expand **Advanced Details**, and paste the script below into **User data**:

```bash
#!/bin/bash
# Install Python3 
sudo yum -y install python36
# Install boto3 and awscli
sudo pip install boto3 awscli
# Download Deployed files
wget https://pdm-data.s3.amazonaws.com/pdm_resource.tar
tar -xvf pdm_resource.tar -C /home/ec2-user
```

8. Select **Next: Add Storage** -> **Next: Add Tags** -> **Next: Configure Security Group**
> (Optional) You can add tags to specify the instance.

9. ☑ **Select an existing security group**, and select **WebSecurityGroup**.

10. Select **Review and Launch** -> **Launch**.

11. You can **Choose an existing key pair** or **Create a new key pair**, ☑ **I acknowledge....**, and select **Launch Instances**.

12. Because we have launch the instance with an ***user data***, it might take ***3 - 5*** minutes to launch, you can click **View Instances** to check the instance's status.

### Connect to your instance

Here we use **PuTTY** to connect to the instance. The first we have to convert **.pem private key** to **.ppk private key** by **PuTTYgen**.

1. Open **PuTTYgen** and Click **Load** to import the **.pem key** you downloaded before.

2. Click **Generate** to generate .ppk key.

3. Click  **Save private key** to save the .ppk key.

For now we get the **.ppk private key**, so we start to connect to the instance.

4. Open **PuTTY**.

5. In the **Host Name** box, enter `ec2-user@<Your-public-dns-name>`
> You can find public dns name in **Description** block below.

<p align="center">
    <img src="images/putty_1.JPG" width="80%" height="80%">
</p>

6. In the **Category** pane, expand **Connection**, expand **SSH**, and then choose **Auth**.

7. Click **Browse** and choose **.ppk private key** which you created before.

<p align="center">
    <img src="images/putty_2.JPG" width="50%" height="50%">
</p>

8. Click **Open** below.

### Setup AWS Credential On EC2

First, you have to find your AWS Credential before you setup it on EC2. 

1. Click on your user name at the top right of the page.

2. Click on the **My Security Credentials** link from the drop-down menu

<p align="center">
    <img src="images/credential_1.JPG" width="50%" height="50%">
</p>

3. Scroll down and find the **Access keys for CLI, SDK, & API access** section, and click **Create access key**.

<p align="center">
    <img src="images/credential_2.JPG" width="80%" height="80%">
</p>

4. **Remember** your access key and **Download .csv file**, you can check the access key in .csv file in the future.

<p align="center">
    <img src="images/credential_3.JPG" width="80%" height="80%">
</p>

5. After you get the access key, we can setup AWS Credential On EC2 now.

```bash
$ aws configure
```
- AWS Access Key ID [None]: `<Your-Access-Key-ID>`
- AWS Secret Access Key [None]:` <Your-Secret-Access-Key>`
- Default region name [None]: `us-east-1`
- Default output format [None]:` json`

### Deploy Necessary Resource

<p align="center">
    <img src="images/architecture_2.JPG" width="80%" height="80%">
</p>

1. Because we use **python3** to run deployed file, we install some necessary package. Follow the commands below:
```bash
# Install pip3
$ wget https://bootstrap.pypa.io/get-pip.py
$ python3 get-pip.py --user
$ source ~/.bash_profile

# Install boto3
$ pip3 install boto3 --user
```

2. Run command to deploy resource.
```bash
$ python3 Scripts/resource_setup.py
...
 make_bucket: <SAGEMAKER_BUCKET> #Remember this bucket name, it will be used in this lab frequently.
...
```
The process will create resources below:
- S3 Bucket/Object
    - s3://<SAGEMAKER_BUCKET>
    - s3://<SAGEMAKER_BUCKET>/scripts/emr-install-my-jupyter-libraries.sh
    - s3://<SAGEMAKER_BUCKET>/scripts/samgemaker-notebook-setup.sh
- DynamoDB
    - telemetry_db
    - logs_db
    - realtime_cycles
    - realtime_features
    - realtime_predictions
- Kinesis Stream
    - telemetry_stream
    - logs_stream

### Create Lambda Function
1. First we create an IAM role for lambda functions.
- On the **Service** menu, select **IAM**
- On the **left navigation pane**, select **Roles**.
- Click **Create role**

<p align="center">
    <img src="images/lambda_1.JPG" width="80%" height="80%">
</p>

- Choose **Lambda**, then click **Next: Permissions**.

<p align="center">
    <img src="images/lambda_2.JPG" width="80%" height="80%">
</p>

- In **Filter policies**, enter **AmazonKinesisFullAccess** to search policy, and select **AmazonKinesisFullAccess**.

- Follow previous step, select policies: **AmazonDynamoDBFullAccess, AmazonSageMakerFullAccess**.

<p align="center">
    <img src="images/lambda_3.JPG" width="80%" height="80%">
</p>

- Click **Next: Tags** -> **Next: Review**.
- Fill `pdm_lambda_role` in **Role name**, then **Create role**.
> To be aware of your policies is correct.
<p align="center">
    <img src="images/lambda_4.JPG" width="80%" height="80%">
</p>

2. On the service menu, select **Lambda**.

3. In the navigation pane, click **Functions**.

4. Click **Create function**.

5. Choose **Author from scratch**.

<p align="center">
    <img src="images/lambda_5.JPG" width="80%" height="80%">
</p>

6. Enter the following details and click **Create function** :

- Function name : `telemetry_lambda`
- Runtime : `Python 3.6`
- Execution role : `pdm_lambda_role`

<p align="center">
    <img src="images/lambda_6.JPG" width="80%" height="80%">
</p>

7. Follow previous steps to create two functions with following details:

    7.1 Logs
    - Function name : `logs_lambda`
    - Runtime : `Python 3.6`
    - Execution role : `pdm_lambda_role`

    7.2 Prediction
    - Function name : `pdm_predict_lambda`
    - Runtime : `Python 3.6`
    - Execution role : `pdm_lambda_role`

8. Back to terminal and run command to upload codes to lamdba functions and creat IoT topic.

```bash
$ python3 Scripts/configure_lambda.py
```

9. Back to **AWS Lambda**, click into **telemetry_lambda** function you just created.

10. Click **Add trigger**.

<p align="center">
    <img src="images/lambda_7.JPG" width="80%" height="80%">
</p>

11. Select **AWS IoT** and enter the following details.

- IoT type : `Custom IoT rule`
- Rule : `telemetry_rule`


<p align="center">
    <img src="images/lambda_8.JPG" width="70%" height="70%">
</p>

12. Click **Add**.

13. Follow previous steps to add trigger with following details for **logs_lambda**:

- IoT type : `Custom IoT rule`
- Rule : `logs_rule`

14. Click into **pdm_predict_lambda** function.

15. Click **Add trigger**.

16. Select **DynamoDB** and enter the following details.

- DynamoDB table : `realtime_features`
- Batch size : `100`
- Leave other settings as default, then click **Add**.

<p align="center">
    <img src="images/lambda_9.JPG" width="70%" height="70%">
</p>

### Use Elastic Beanstalk to Deploy Flask App

1. On the service menu, select **Elastic Beanstalk**.
2. Click **Create New Application**.

<p align="center">
    <img src="images/eb_1.JPG" width="80%" height="80%">
</p>

3. Fill `pdm` in **Application Name**, then **Create**.

<p align="center">
    <img src="images/eb_2.JPG" width="50%" height="50%">
</p>

4. Click **Actions** -> Create **environment**.

<p align="center">
    <img src="images/eb_3.JPG" width="80%" height="80%">
</p>

5. Select **Web server environment**, then **Select**.

6. Enter the following details in **Environment information** zone.

- Environment name : `Pdm-env`
- Domain : `pdm-<Your-Name>`
> Ex: `pdm-dennis`

<p align="center">
    <img src="images/eb_4.JPG" width="70%" height="70%">
</p>

7. Enter the following details in **Base configuration** zone.

- Platform : `Python in Preconfigured`

<p align="center">
    <img src="images/eb_5.JPG" width="70%" height="70%">
</p>

- Application code : `Upload your code`

8. Click **Upload**, then choose the `pdm_2019/bin/pdm.zip` file you downloaded in the beginning -> click **Upload**.

<p align="center">
    <img src="images/eb_6.JPG" width="50%" height="50%">
</p>

<p align="center">
    <img src="images/eb_7.JPG" width="80%" height="80%">
</p>

9. Click **Create environment**.

> It takes about 7 minutes to create.

10. After creation, you can check the environment URL whether is able to use.

<p align="center">
    <img src="images/eb_10.JPG" width="80%" height="80%">
</p>

11. We have to edit **aws-elasticbeanstalk-ec2-role** policies in **IAM**.

- On the service menu, select **IAM**.
- On the left navigation pane, select **Roles**.
- Search `aws-elasticbeanstalk-ec2-role` and click it.

<p align="center">
    <img src="images/eb_8.JPG" width="100%" height="100%">
</p>

- Click **Attach policies**.

<p align="center">
    <img src="images/eb_9.JPG" width="80%" height="80%">
</p>

- Search `AWSIoTFullAccess` and `AWSIoTFullAccess`, tick them and Click **Attach policy**.

### Add AWS EMR Spark Cluster

<p align="center">
    <img src="images/architecture_3.JPG" width="60%" height="60%">
</p>


1. On the service menu, select **EMR**.
2. On the left navigation pane, select **Clusters**.
3. Click **Create cluster**.
4. Click **Go to advanced options**.

<p align="center">
    <img src="images/emr_1.JPG" width="80%" height="80%">
</p>

5. Select `emr-5.19.0` and tick `Spark 2.3.2, Livy 0.5.0, Hive 2.3.3` in **Software Configuration**.

<p align="center">
    <img src="images/emr_2.JPG" width="80%" height="80%">
</p>

6. Leave other settings as default, and click **Next**.

7. Enter the following details in **Hardware Configuration**.

- Instance group configuration : `Uniform instance groups`
- Network : `Lab VPC`
- EC2 Subnet : `Lab Public Subnet 1`
- Root device EBS volume size : `10`

<p align="center">
    <img src="images/emr_3.JPG" width="80%" height="80%">
</p>

8. Change the **Instance type** to `m4.large` for **Master** and **Core** node type, and cancel the **Task** node type.

<p align="center">
    <img src="images/emr_4.JPG" width="80%" height="80%">
</p>

9. Click **Next**.

10. Enter the following details in **General Options**.

- Cluster name : `pdm-spark-sagemaker`

11. Scroll down to expand **Bootstrap Actions** and select `Custom action` in **Add bootstrap action**.

<p align="center">
    <img src="images/emr_5.JPG" width="100%" height="100%">
</p>

12. Click **Configure and add**, and enter the following details.

- Name : `install related libraries`
- Script location : `Select <SAGEMAKER_BUCKET>/scripts/emr-install-my-jupyter-libraries.sh`

>i.e. `pdm-sagemaker-xxxxxxxxxx/scripts/emr-install-my-jupyter-libraries.sh`

<p align="center">
    <img src="images/emr_6.JPG" width="60%" height="60%">
</p>

13. Click **Add**.

<p align="center">
    <img src="images/emr_8.JPG" width="80%" height="80%">
</p>

14. Click **Next**.

15. Select **EC2 key pair** that you own in **Security Options**.


<p align="center">
    <img src="images/emr_7.JPG" width="80%" height="80%">
</p>

16. Leave other settings as default, click **Create cluster**.

17. After creation, we are going to find EMR private IP of master node, then edit `pdm_2019/Notebooks/config.json` file, we will use it later.

- Click **Hardware**.
- Click into **MASTER** instance.

<p align="center">
    <img src="images/emr_9.JPG" width="80%" height="80%">
</p>

- Scroll right and copy the private IP.

<p align="center">
    <img src="images/emr_10.JPG" width="80%" height="80%">
</p>

- Open `pdm_2019/Notebooks/config.json` file and replace your private IP to `172.31.32.171`, then save the file.

<p align="center">
    <img src="images/emr_11.JPG" width="50%" height="50%">
</p>

### Set EMR Security Group

1. On the service menu, select **EC2**.

2. On the left navigation pane, select **Security Groups**.

3. Click **Create Secutity Group** and enter the following details.

- Security group name : `pdm-spark-sagemaker`
- VPC : `Lab VPC`

<p align="center">
    <img src="images/sg_1.JPG" width="80%" height="80%">
</p>

4. Click **Create**.

5. Copy the **Group ID** of **pdm-spark-sagemaker**, we will use it later.

<p align="center">
    <img src="images/sg_2.JPG" width="80%" height="80%">
</p>

6. Search Security Group : `ElasticMapReduce-master`, and click **Inbound** -> click **Edit**.

<p align="center">
    <img src="images/sg_3.JPG" width="80%" height="80%">
</p>

7. Click **Add Rule** and enter the following details:

    7.1 Livy port

    - Type : `Custom TCP Rule`
    - Port Range : `8998`
    - Source : `Paste the Group ID you copy before`

    7.2 SSH

    - Type : `SSH`
    - Port Range : `22`
    - Source : `0.0.0.0/0`(Anywhere)

<p align="center">
    <img src="images/sg_4.JPG" width="80%" height="80%">
</p>

### Create AWS SageMaker Notebook Instance

1. On the service menu, select **Amazon SageMaker**.

2. On the left navigation pane, select **Notebook instances**.

3. Click **Create notebook instance**.

4. Enter the following details in **Notebook instance settings**:

- Notebook instance name : `PDMNotebookInstance`
- Notebook instance type : `ml.t2.medium`

<p align="center">
    <img src="images/11_1.JPG" width="80%" height="80%">
</p>

5. Select `Create a new role` in **IAM role**, and enter `<SAGEMAKER_BUCKET>` in **Specific S3 buckets**.

<p align="center">
    <img src="images/11_2_1.JPG" width="80%" height="80%">
</p>

6. Click **Create role**.

7. Now we attach **AmazonDynamoDBFullAccess** policy to this role, Click **AmazonSageMaker-ExecutionRole-xxxxxxxxx**.

<p align="center">
    <img src="images/11_1_2.JPG" width="80%" height="80%">
</p>

8. Click **Attach policies**.

<p align="center">
    <img src="images/11_2_2.JPG" width="80%" height="80%">
</p>

9. Search **AmazonDynamoDBFullAccess** and tick it, click **Attach policy**.

<p align="center">
    <img src="images/11_2_3.JPG" width="80%" height="80%">
</p>

10. Back to create notebook instance page, expand **Network**.

11. Enter the following details in **Network**:

- VPC : `Lab VPC`
- Subnet : `Lab Public Subnet 1`
- Security group(s) : `pdm-spark-sagemaker`

<p align="center">
    <img src="images/11_3.JPG" width="80%" height="80%">
</p>

12. Click **Create notebook instance**.

### Set AWS SageMaker Notebook Instance Environment

1. Click **Open Jupyter**.

<p align="center">
    <img src="images/13_1.JPG" width="80%" height="80%">
</p>

2. Click **Upload**.

<p align="center">
    <img src="images/13_2.JPG" width="80%" height="80%">
</p>

3. Select all files in `pdm_2019/Notebooks`.

4. Click **Upload** on each file.

<p align="center">
    <img src="images/13_3.JPG" width="100%" height="100%">
</p>

5. Click **New** -> **Terminal**.

<p align="center">
    <img src="images/13_4.JPG" width="100%" height="100%">
</p>

6. Run command to move **config.json** file to **.sparkmagic** directory.

```bash
$ mv SageMaker/config.json .sparkmagic/
```

7. Run command to execute `samgemaker-notebook-setup.sh` file.

```bash
$ sudo bash SageMaker/sagemaker-notebook-setup.sh
```

8. Close the Terminal page.

### Build ML Model

1. Generate Data for training model: 

- Open **DataGeneration.ipynb** file in **jupyter**. [kernel: conda_python3]
- Find `bucket` in the code and replace it to **<SAGEMAKER_BUCKET>**.

```
bucket = '<SAGEMAKER_BUCKET>'
```

<p align="center">
    <img src="images/14_1.JPG" width="80%" height="80%">
</p>

- Click **Cell** -> **Run All**  

<p align="center">
    <img src="images/14_4.JPG" width="80%" height="80%">
</p>

> It takes about 10 minutes to generate.

2. Generate Feature of Data for traning model: 

- Open **FeatureEngineering.ipynb** file in **jupyter**. [kernel: PySpark]
- Find `bucket` in the code and replace it to **<SAGEMAKER_BUCKET>**.

```
bucket = 's3a://<SAGEMAKER_BUCKET>'
```

<p align="center">
    <img src="images/14_2.JPG" width="80%" height="80%">
</p>

- Click **Cell** -> **Run All**

<p align="center">
    <img src="images/14_4.JPG" width="80%" height="80%">
</p>

> It takes about 10 minutes to generate.

3. Train and deploy model:

- Open **Modeling.ipynb** file in **jupyter**. [kernel: conda_python3]
- Find `bucket` in the code and replace it to **<SAGEMAKER_BUCKET>**.

```
bucket = '<SAGEMAKER_BUCKET>'
```

<p align="center">
    <img src="images/14_3.JPG" width="100%" height="100%">
</p>

- Click **Cell** -> **Run All**

<p align="center">
    <img src="images/14_4.JPG" width="80%" height="80%">
</p>

> It takes about 20 minutes to train and deploy model.

### Setup Featurizer and Test

We will use **FileZilla** to upload `pdm_2019/bin/featurizer_2.11-1.0.jar` file to **spark master node** which created by **AWS EMR** before.

1. Open **FileZillaPortable.exe** which you downloaded in the beginning and click **Edit** -> **Settings**

<p align="center">
    <img src="images/15_6.JPG" width="80%" height="80%">
</p>

2. On the left navigation pane, select **SFTP**.

<p align="center">
    <img src="images/15_1.JPG" width="80%" height="80%">
</p>

3. Click **Add key file** and select **.ppk key file** which you selected when you created AWS EMR spark cluster.

4. Click **OK**.

5. Back to AWS page, on the service menu, select **EMR**.

6. Click into **pdm-spark-sagemaker** cluster you created.

7. Click **SSH**

<p align="center">
    <img src="images/15_2.JPG" width="100%" height="100%">
</p>

8. Copy the host name of master node.

<p align="center">
    <img src="images/15_3.JPG" width="100%" height="100%">
</p>

9. Back to **FileZilla**, paste the host name of master node to **Host**, and fill **port** in `22`.

<p align="center">
    <img src="images/15_4.JPG" width="100%" height="100%">
</p>

10. Click **Quick Connect**.

11. After connecting, change directory to `pdm_2019/bin` on **Local site**(left zone). And change directory to `/home/hadoop` on **Remote site**(right zone).

<p align="center">
    <img src="images/15_5.JPG" width="80%" height="80%">
</p>

12. Drag **featurizer_2.11-1.0.jar** file from **Local site** to **Remote site**, then it will upload file to spark master node.

13. Now connect the **PdM App URL** which used **Elastic Beanstalk** to create.

<p align="center">
    <img src="images/eb_10.JPG" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/test_2.JPG" width="80%" height="80%">
</p>

14. Click **裝置模擬** -> **新增裝置**.

    14.1 test_1
    
    - 裝置ID : `test_1`
    - 模擬參數 (JSON) :
    ```
    {
    "h1": 0.85,
    "h2": 0.82
    }
    ```

    14.2 test_2

    - 裝置ID : `test_2`
    - 模擬參數 (JSON) :
    ```
    {
    "h1": 0.73,
    "h2": 0.71
    }
    ```

    14.3 test_3

    - 裝置ID : `test_3`
    - 模擬參數 (JSON) :
    ```
    {
    "h1": 0.68,
    "h2": 0.63
    }

<p align="center">
    <img src="images/test_1.JPG" width="60%" height="60%">
</p>

>The h1/h2 parameters each represent a device health, and h1 has a slower rate of decline. If it is set to 0.8, it will fall to 0 after a few hours. h2 drops rapidly, it starts to fall to 0 in less than half an hour from 0.6. As h1 and h2 fall faster and faster, the prediction results will gradually evolve from normal to predict devices unnormal. If h2 starts at 0.7, the first prediction will be generated after 20 minutes, and then the result will be updated continuously, and unnormal situation can be predicted 30 to 60 minutes before h2 falls to 0. In addition, after the device generates an error, it will be reset to the h1/h2 parameter that was originally set to start the simulation again.

15. Use PuTTY to connect to **Spark Master Node**.

- Open **PuTTY**.

- In the **Host Name** box, enter `hadoop@<Your-Master-Public-DNS>`

<p align="center">
    <img src="images/15_3.JPG" width="100%" height="100%">
</p>

- In the **Category** pane, expand **Connection**, expand **SSH**, and then choose **Auth**.

- Click **Browse** and choose **.ppk private key** which you own.

<p align="center">
    <img src="images/putty_2.JPG" width="60%" height="60%">
</p>

- Click **Open** below.

16. Run command in **Spark Master terminal**:

```bash
$ sudo -u hdfs spark-submit featurizer_2.11-1.0.jar us-east-1 realtime_cycles realtime_features <your_aws_access_key_id> <your_aws_secret_key>
```
> If you see the error message about `log4j`, it's normal.

> You can see the prediction result after 20 minutes, so you have to wait it.

17. After 20 minutes, go to the **PdM Webapp** and check the predictions.

<p align="center">
    <img src="images/test_6.JPG" width="40%" height="40%">
</p>


### Result Examples

<p align="center">
    <img src="images/test_3.JPG" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/test_4.JPG" width="80%" height="80%">
</p>

<p align="center">
    <img src="images/test_5.JPG" width="80%" height="80%">
</p>

### Conclusion

Congratulations! Now you have learned how to use AWS AI modules(Amazon SageMaker / EMR /
Lambda / S3 / Kinesis / Beanstalk / DynamoDB). And have learned about generating data, feature extraction, training models, models deployment, and using training models to deploy website.

### Clean up

- Lambda Functions
- DynamoDB Tables
- EC2 Instance
- EMR Cluster
- S3 Bucket
- Elastic Beanstalk Application
- IAM Roles
- Sagemaker Notebook Instance and Endpoint
- Cloudformation Stack
- Kinesis Data Streams
